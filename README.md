# cAPSMOJI
Tired of the useless caps-lock key on your keyboard? Turn it to something useful. With cAPSMOJI, you can display an emoji selector by pressing the caps-lock key.

## Prerequisites
You need to have Windows 10 1803 or newer installed. Additionally, a keyboard with a capslock key is required.

## How to run
Either download the cAPSMOJI.ahk script from this repository, install [Autohotkey](https://www.autohotkey.com/) and run it by double clicking.

OR

Download the binary from the [releases](https://gitlab.com/MoeRT09/capsmoji/-/releases) page, extract the ZIP file and and run the `cAPSMOJI.exe`.

If you want it to automatically launch it on boot, put it in your autostart directory:
- Press `Windows key` + `R` to open the "run" dialog.
- Enter `shell:startup` and press `enter`
- An Explorer window will open. Put the script or the executable there.